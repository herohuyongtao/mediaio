#include <opencv2/opencv.hpp> 

#include "mediaioAPI.h"

int main()
{
	mediaioAPI mediaio_API("-your-video-file-.avi");
	
	unsigned long long num_frames = mediaio_API.get_num_frames();
	for (unsigned long long i=0; i<num_frames; ++i){	
		cv::Mat mat = mediaio_API.get_frame_at_index(i);
		cv::imshow("", mat);
		cv::waitKey(30);
	}

	return 0;
}