// mediaio.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "mediaio.h"

#include "Clip.h"
#include "Camera.h"

#include <fstream>
using namespace std;

#include <hash_map>
using namespace stdext;

#include <Wmcodecdsp.h>
#include <atlbase.h>
#include <Mftransform.h>

#include <Gdiplus.h>
using namespace Gdiplus;

wstring toString(GUID id)
{
	wchar_t idstr[50];
	StringFromGUID2(id, idstr, 50);
	return wstring(idstr);
}

//void mioAddCam(GUID camID, wchar_t*	camName)
//{
//	CCamera* cam=new CCamera(toString(camID), camName);
//}

void mioDelCam(GUID camID)
{
	CCamera::delCamera(toString(camID));
}

// check file existed or not
bool checkFileExisted(const wchar_t *path)
{
	ifstream ifs(path);
	if (ifs.good())
	{
		ifs.close();
		return true;
	}
	else
	{
		ifs.close();
		return false;
	}
}

HRESULT mioAddClip(GUID clipID, const wchar_t*	clipFilePath)
{
	// check file does exist
	if (!checkFileExisted(clipFilePath))
		return E_FAIL;

	/*CClip* clip=*/new CClip(toString(clipID), clipFilePath);
	return S_OK;
}

HRESULT mioAddClipWithTimingInfo(GUID clipID, GUID camID, const wchar_t*	clipFilePath,
	UINT64		clipStartTime, UINT64		clipEndTime)
{
	// check file does exist
	if (!checkFileExisted(clipFilePath))
		return E_FAIL;

	CClip* clip=new CClip(toString(clipID), clipFilePath, clipStartTime, clipEndTime);
	clip->setCamera(toString(camID));
	return S_OK;
}

void mioDelClip(GUID clipID)
{
	CClip::delClip(toString(clipID));
}

void mioUpdateClip(GUID clipID, GUID camID, UINT64 clipStartTime,  UINT64 clipEndTime )
{
	CClip* clip=CClip::getClip(toString(clipID));
	CCamera* cam=CCamera::getCamera(toString(camID));

	if (clip->camera!=cam){
		if (clip->camera!=NULL){
			clip->camera->removeClipFromCamera(clip);
		}
		if (cam!=NULL){
			cam->addClipToCamera(clip);
			clip->camera=cam;
		}
	}

	clip->startTime=clipStartTime;
	clip->endTime=clipEndTime;
}

void mioCleanupAll()
{
	CClip::clearAllClips();
	CCamera::clearAllCameras();
}

ULONG_PTR(gdiplusToken);

HRESULT mioInitializeMediaIO()
{
	HRESULT hr = S_OK;

	IFC( hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE) );
	IFC( hr = MFStartup(MF_VERSION) );

	GdiplusStartupInput gdiplusstartupinput;
	GdiplusStartup(&gdiplusToken, &gdiplusstartupinput, NULL);

Cleanup:
	return hr;
}

HRESULT mioShutdownMediaIO()
{
	HRESULT hr = S_OK;

	GdiplusShutdown(gdiplusToken);

	IFC( hr = MFShutdown() );
	CoUninitialize();
Cleanup:
	return hr;
}

HRESULT mioGetClipInfo(wchar_t* clipID,
	UINT32& framewidth, UINT32& frameheight, UINT32& par_numerator, UINT32& par_denominator,
	UINT32& fps_numerator, UINT32& fps_denominator, UINT32& framecount, UINT64& duration, bool&	topdown)
{
	HRESULT hr;
	CClip* clip=CClip::getClip(clipID);
	if (clip==NULL) return E_FAIL;
	bool closeafterward=false;
	if (clip->reader!=NULL) closeafterward=true;
	hr=clip->readVideoInfo();
	if (closeafterward) SAFE_RELEASE(clip->reader);
	if (hr!=S_OK) return hr;

	framewidth=clip->framewidth;
	frameheight=clip->frameheight;
	par_numerator=clip->par_numerator;
	par_denominator=clip->par_denominator;
	fps_numerator=clip->fps_numerator;
	fps_denominator=clip->fps_denominator;
	framecount=clip->framecount;
	duration=clip->duration;
	topdown=clip->topdown;

	return S_OK;
}

HRESULT mioGetFrameAt(GUID camID, UINT64 hnsPos, UINT8* bitmap, UINT32 size)
{
	HRESULT hr = E_FAIL;
	CCamera* camera=NULL;
	CClip* clip=NULL;

	if ( (camera=CCamera::getCamera(toString(camID))) != NULL)
		if ( (clip=camera->locateClipAtTime(hnsPos)) != NULL){
			IFC( clip->getFrame(hnsPos-clip->startTime, bitmap) );
			return S_OK;
		}

Cleanup:
	memset(bitmap, 0, size);
	return hr;
}

HRESULT mioGetFrameSize(GUID camID, /*UINT64 hnsPos,*/ UINT32& width, UINT32& height)
{
	CCamera* camera=CCamera::getCamera(toString(camID));
	if (camera==NULL) return E_FAIL;
	if (camera->clips.size()<=0) return E_FAIL;
	camera->clips.front()->getFrameSize(width, height);

	return S_OK;
}

HRESULT mioGetFrameOfClipAt(GUID clipID, UINT64 hnsPos, UINT8* bitmap, UINT32 size)
{
	HRESULT hr = E_FAIL;

	CClip* clip=CClip::getClip(toString(clipID));
	if (clip==NULL) return E_FAIL;

	IFC( clip->getFrame(hnsPos, bitmap) );
	return S_OK;
Cleanup:
	memset(bitmap, 0, size);
	return hr;
}

Bitmap* ResizeClone(Bitmap *bmp, INT width, INT height)
{
    UINT o_height = bmp->GetHeight();
    UINT o_width = bmp->GetWidth();
    INT n_width = width;
    INT n_height = height;
    double ratio = ((double)o_width) / ((double)o_height);
    if (o_width > o_height) {
        // Resize down by width
        n_height = static_cast<UINT>(((double)n_width) / ratio);
    } else {
        n_width = static_cast<UINT>(n_height * ratio);
    }
    Gdiplus::Bitmap* newBitmap = new Gdiplus::Bitmap(n_width, n_height, bmp->GetPixelFormat());
    Gdiplus::Graphics graphics(newBitmap);
    graphics.DrawImage(bmp, 0, 0, n_width, n_height);
    return newBitmap;
}

// a standalone frame-reading function for creating thumbnail
HRESULT mioGetThumbnail(wchar_t* clipFilePath, UINT64 thumbnailtime, UINT32& width, UINT32& height, UINT8* bitmap)
{
	HRESULT hr=S_OK;
	IMFMediaType *pType = NULL;
	GUID subtype = { 0 };

	IMFSourceReader *reader = NULL;

	IMFAttributes *pAttributes = NULL;
	IFC( MFCreateAttributes(&pAttributes, 1) );
	IFC( pAttributes->SetUINT32(MF_SOURCE_READER_ENABLE_VIDEO_PROCESSING, TRUE) );
	IFC( MFCreateSourceReaderFromURL(clipFilePath, pAttributes, &reader) );

	IFC( MFCreateMediaType(&pType) );
	IFC( reader->GetNativeMediaType((DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, 0, &pType) );

	UINT32 framewidth, frameheight;
	IFC( MFGetAttributeSize(pType, MF_MT_FRAME_SIZE, &framewidth, &frameheight) );

	UINT32 fps_numerator,fps_denominator;
	IFC( MFGetAttributeSize(pType, MF_MT_FRAME_RATE, &fps_numerator, &fps_denominator) );
	double frameInterval = (double) ((UINT64)((double)fps_denominator / (double) fps_numerator * 10000000));

	IMFMediaType *pOutputType = NULL;	// we need 32 bit RGB pixel format
	IFC( MFCreateMediaType(&pOutputType) );
	IFC( pOutputType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video) );
	IFC( pOutputType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_RGB32) );
	IFC( hr = reader->SetCurrentMediaType( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, NULL, pOutputType) );

	// getting video format
	//LONG lStride = 0;
	IFC( reader->GetCurrentMediaType( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, &pType ) );
	IFC( pType->GetGUID(MF_MT_SUBTYPE, &subtype) );
	if (subtype != MFVideoFormat_RGB32)
	{
		hr = E_UNEXPECTED;
		goto Cleanup;
	}

	IFC( reader->SetStreamSelection( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, TRUE) );

	IMFMediaBuffer *pBuffer = NULL;
	UINT8*		pTempData = NULL;
	DWORD       cbBitmapData = 0;       // Size of data, in bytes
	LONGLONG sampletime=0;

	// get frame
	DWORD       dwFlags = 0;
	//LONGLONG    hnsTimeStamp = 0;
	//DWORD       cSkipped = 0;           // Number of skipped frames
	UINT64		currTime;

	IMFSample *pSample = NULL;

	while (true)
	{
		IMFSample *pSampleTmp = NULL;


		IFC( reader->ReadSample( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM,
			0, NULL, &dwFlags, &sampletime, &pSampleTmp) );

		if (dwFlags & MF_SOURCE_READERF_ENDOFSTREAM) break;

		if (dwFlags != 0){
			MessageBox(NULL,"Error","Error", MB_OK);
		}

		if (pSampleTmp == NULL) continue;
		if (thumbnailtime>(UINT64)sampletime && (thumbnailtime - (UINT64)sampletime)>frameInterval / 2){
			SAFE_RELEASE(pSampleTmp);
			continue;
		}

		//SAFE_RELEASE(currsample);

		pSample = pSampleTmp;
		pSample->AddRef();
		currTime=sampletime;

		SAFE_RELEASE(pSampleTmp);
		break;
	}

    if (pSample)
    {
        IFC( pSample->ConvertToContiguousBuffer(&pBuffer) );
        IFC( pBuffer->Lock(&pTempData, NULL, &cbBitmapData) );

		Bitmap bmp((INT)framewidth, (INT)frameheight, (INT)framewidth*4, PixelFormat32bppRGB, pTempData);
		Bitmap *thumbnail=ResizeClone(&bmp, 128, 128);

		width=thumbnail->GetWidth();
		height=thumbnail->GetHeight();

		Gdiplus::Rect rect(0, 0, width, height);


		BitmapData bmd;
		/*Status s=*/thumbnail->LockBits(&rect, ImageLockModeRead, PixelFormat32bppRGB, &bmd);

		memcpy(bitmap, bmd.Scan0, bmd.Stride*bmd.Height);
		thumbnail->UnlockBits(&bmd);
		pBuffer->Unlock();
    }
    else
    {
        hr = MF_E_END_OF_STREAM;
    }

Cleanup:
	SAFE_RELEASE(reader);
	SAFE_RELEASE(pType);
    SAFE_RELEASE(pBuffer);
	return hr;
}

UINT8 * ReverseRGB_Row(UINT8 *bmp, UINT32 width, UINT32 height)
{
	UINT8 *buf;

	int stride = sizeof(UINT8) * width * 4;

	buf = (UINT8*)malloc(stride * height);
	memset(buf, 0, stride * height);

	for(unsigned int j=0;j<height;j++)
	{
		memcpy(buf + j*stride, bmp + (height-1-j)*stride, stride);
	}

	return buf;
}

void SaveBitmapToFile( BYTE* pBitmapBits, LONG lWidth, LONG lHeight, WORD wBitsPerPixel, char* fileName )
{
	BITMAPFILEHEADER bfh = {0};
	BITMAPINFOHEADER bmpInfoHeader = {0};
	HANDLE hFile;
	DWORD dwWritten;

	LONG width2 = (wBitsPerPixel/8) * lWidth;
	width2 = width2 + (4 - (width2 % 4));

	bmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER); // Set the size
	bmpInfoHeader.biBitCount = wBitsPerPixel;        // Bit count
	bmpInfoHeader.biClrImportant = 0;                // Use all colors
	bmpInfoHeader.biClrUsed = 0; // Use as many colors according to bits per pixel
	bmpInfoHeader.biCompression = BI_RGB;    // Store as un Compressed
	bmpInfoHeader.biHeight = lHeight;  // Set the height in pixels
	bmpInfoHeader.biWidth = lWidth;  // Width of the Image in pixels
	bmpInfoHeader.biPlanes = 1;  // Default number of planes

	// Calculate the image size in bytes
	bmpInfoHeader.biSizeImage = width2 * lHeight;

	// This value should be values of BM letters i.e 0��4D42
	// 0��4D = M 0��42 = B storing in reverse order to match with endian
	bfh.bfType=0x4D42;
	// Offset to the RGBQUAD
	bfh.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
	// Total size of image including size of headers
	bfh.bfSize = bfh.bfOffBits + bmpInfoHeader.biSizeImage;
	// Create the file in disk to write
	hFile = CreateFile( fileName,GENERIC_WRITE, 0,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);

	if( !hFile ) // return if error opening file
	{
		return;
	}

	dwWritten = 0;
	// Write the File header
	WriteFile( hFile, &bfh, sizeof(bfh), &dwWritten , NULL );
	// Write the bitmap info header
	WriteFile( hFile, &bmpInfoHeader, sizeof(bmpInfoHeader), &dwWritten, NULL );
	// Write the RGB Data
	WriteFile( hFile, pBitmapBits, bmpInfoHeader.biSizeImage, &dwWritten, NULL );
	// Close the file handle
	CloseHandle( hFile );
}

void mioSaveToBMP(UINT8* data, 
	char* fileName, 
	UINT32 width, 
	UINT32 height, 
	bool topdown)
{
	UINT8 * buf = data;
	if (topdown)
		buf = ReverseRGB_Row(data, width, height);
	SaveBitmapToFile((BYTE*) buf, width, height, 32, fileName);
	free(buf);
}

UINT64 mioGetFrameSpanBasedOnFPS(UINT32 fps_numerator, UINT32 fps_denominator)
{
	return (UINT64) floor((double)fps_denominator*10000000/(double)fps_numerator + 0.5);
}