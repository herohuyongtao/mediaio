#pragma once

#include <string>
#include <hash_map>
#include <mfapi.h>
#include <mfidl.h>
#include <mfreadwrite.h>
#include <mferror.h>
#include <WinNT.h>

class CCamera;

class CClip
{
public:
	CClip(std::wstring id, std::wstring path);
	CClip(std::wstring id, std::wstring path, UINT64 stime, UINT64 etime);
	~CClip();

	static CClip* getClip(std::wstring id);
	static void delClip(std::wstring id);
	static void clearAllClips();

	void setCamera(std::wstring id);
	HRESULT	getFrame(UINT64 cliptime, UINT8* bitmap);
	HRESULT	getFrameSize(UINT32& width, UINT32& height);
	HRESULT	readVideoInfo();

	std::wstring guid;
	std::wstring cameraguid;
	std::wstring filePath;

	CCamera* camera;

	UINT32 bitmapSize;
	UINT64 frameInterval;
	UINT32 framewidth;
	UINT32 frameheight;
	UINT32 par_numerator;
	UINT32 par_denominator;
	UINT32 fps_numerator;
	UINT32 fps_denominator;
	UINT32 framecount;
	UINT64 duration;
	bool topdown;
	bool canseek;

	UINT64 startTime;
	UINT64 endTime;
	UINT64 currTime;
	int compareTime(UINT64 x, UINT64 y);

    IMFSourceReader *reader;
	IMFTransform *converter;
	IMFSample *currsample;
};

