#include "StdAfx.h"
#include "Clip.h"
#include "Camera.h"

#include <string>
using namespace std;

#include <hash_map>
using namespace stdext;

#include <Wmcodecdsp.h>

static hash_map<wstring, CClip*> clips;

CClip::CClip(wstring id, wstring path) :
		guid(id), filePath(path), startTime(0), endTime(300000000), reader(NULL), currTime(0), canseek(false), currsample(NULL), camera(NULL)
{
	clips[guid]=this;
	converter=NULL;
}

CClip::CClip(wstring id, wstring path, UINT64 stime, UINT64 etime) :
		guid(id), filePath(path), startTime(stime), endTime(etime), reader(NULL), currTime(0), canseek(false), currsample(NULL), camera(NULL)
{
	clips[guid]=this;
	converter=NULL;
}

CClip::~CClip(void){
	if (camera!=NULL) camera->removeClipFromCamera(this);
	SAFE_RELEASE(currsample);
	SAFE_RELEASE(reader);
}

CClip* CClip::getClip(std::wstring id)
{
	return clips[id];
}

void CClip::delClip(std::wstring id)
{
	delete clips[id];
	clips[id]=NULL;
}

void CClip::setCamera(std::wstring id)
{
	cameraguid=id;
	camera=CCamera::getCamera(cameraguid);
	if (camera!=NULL) camera->addClipToCamera(this);
}

HRESULT	CClip::readVideoInfo()
{
	HRESULT hr=S_OK;
	IMFMediaType *pType = NULL;
	GUID subtype = { 0 };

	IMFAttributes *pAttributes = NULL;
	IFC( MFCreateAttributes(&pAttributes, 1) );
	IFC( pAttributes->SetUINT32(MF_SOURCE_READER_ENABLE_VIDEO_PROCESSING, TRUE) );
	IFC( MFCreateSourceReaderFromURL(filePath.c_str(), pAttributes, &reader) );

	IFC( MFCreateMediaType(&pType) );
	IFC( reader->GetNativeMediaType((DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, 0, &pType) );

	IMFMediaType *pOutputType = NULL;	// we need 32 bit RGB pixel format
	IFC( MFCreateMediaType(&pOutputType) );
	IFC( pOutputType->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video) );
	IFC( pOutputType->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_RGB32) );
	IFC( hr = reader->SetCurrentMediaType( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, NULL, pOutputType) );
	//if (FAILED(hr)) //decoder cannot produce RGB32, use a DSP converter
	//{
	//	IFC( CoCreateInstance(CLSID_CColorConvertDMO, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&converter)) );

	//	DWORD instmcnt=0, outstmcnt=0;
	//	IFC( converter->GetStreamCount(&instmcnt, &outstmcnt) );
	//	DWORD instmid=0, outstmid=0;
	//	converter->GetStreamIDs(instmcnt, &instmid, outstmcnt, &outstmid);
	//	converter->SetInputType(0, pType, 0);
	//	converter->SetOutputType(0, pOutputType, 0);

	//	MFT_INPUT_STREAM_INFO instminfo;
	//	converter->GetInputStreamInfo(0, &instminfo);
	//	MFT_OUTPUT_STREAM_INFO outstminfo;
	//	converter->GetOutputStreamInfo(0, &outstminfo);

	//}


	IFC( reader->SetStreamSelection( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, TRUE) );

	// getting video format
	LONG lStride = 0;
	IFC( reader->GetCurrentMediaType( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM, &pType ) );
	IFC( pType->GetGUID(MF_MT_SUBTYPE, &subtype) );
	if (subtype != MFVideoFormat_RGB32)
	{
		hr = E_UNEXPECTED;
		goto Cleanup;
	}

	IFC( MFGetAttributeSize(pType, MF_MT_FRAME_SIZE, &framewidth, &frameheight) );
	IFC( MFGetAttributeSize(pType, MF_MT_FRAME_RATE, &fps_numerator, &fps_denominator) );
	lStride = (LONG)MFGetAttributeUINT32(pType, MF_MT_DEFAULT_STRIDE, 1);
	topdown = (lStride > 0);
	frameInterval = (UINT64)((double)fps_denominator / (double) fps_numerator * 10000000);

	MFRatio par;
	par_numerator=par_denominator=1;
	IFC( MFGetAttributeRatio(pType, MF_MT_PIXEL_ASPECT_RATIO, (UINT32*)&par.Numerator, (UINT32*)&par.Denominator) );
	par_numerator=par.Numerator;
	par_denominator=par.Denominator;

	PROPVARIANT dur;
	duration=0;
	IFC( hr = reader->GetPresentationAttribute((DWORD)MF_SOURCE_READER_MEDIASOURCE, MF_PD_DURATION, &dur ) );
	IFC( hr = PropVariantToUInt64(dur, &duration) );

	framecount=(int)floor((double)duration/1e7*(double)fps_numerator/(double)fps_denominator+0.5);



	// check if the clip supports seeking
	ULONG flags = 0;
	PROPVARIANT var;
	PropVariantInit(&var);
	canseek = false;
	IFC( reader->GetPresentationAttribute( (DWORD)MF_SOURCE_READER_MEDIASOURCE,
		MF_SOURCE_READER_MEDIASOURCE_CHARACTERISTICS, &var) );
	IFC( PropVariantToUInt32(var, &flags) );
	if (flags & MFMEDIASOURCE_CAN_SEEK) // && !(flags & MFMEDIASOURCE_HAS_SLOW_SEEK))
	{
		canseek = true;
	}

Cleanup:
	SAFE_RELEASE(pType);
	return hr;
}


HRESULT	CClip::getFrameSize(UINT32& width, UINT32& height)
{
	HRESULT hr=S_OK;

	if (reader==NULL){
		IFC( readVideoInfo() );
	}

	width=framewidth;
	height=frameheight;

Cleanup:
	return hr;
}

void CClip::clearAllClips()
{
	for(hash_map<wstring, CClip*>::iterator i=clips.begin(); i!=clips.end(); i++) delete (*i).second;

	clips.clear();
}

int CClip::compareTime(UINT64 x, UINT64 y)
{
	double fi=(double)frameInterval;
	double xt=(double)x;
	double yt=(double)y;
	if (fabs(xt-yt)<fi/10) return 0;
	if (xt>yt) return 1;
	else return -1;
}


HRESULT	CClip::getFrame(UINT64 cliptime, UINT8* bitmap)
{
	HRESULT hr=S_OK;
	IMFMediaBuffer *pBuffer = NULL;
	UINT8*		pTempData = NULL;
	DWORD       cbBitmapData = 0;       // Size of data, in bytes
	LONGLONG sampletime=0;

	if (reader==NULL){
		IFC( readVideoInfo() );
	}

	bool frameexists=false;
	if (currsample!=NULL){
		if (fabs((double)cliptime-(double)currTime)<(double)frameInterval/2){
			frameexists=true;
		}
	}

	if (!frameexists){
		// seek
		bool needseek=false;

		if (cliptime<=currTime || cliptime-currTime>5e7 || currsample==NULL){
			needseek=true;
		}

		if (needseek){
			PROPVARIANT var;
			PropVariantInit(&var);
			var.vt = VT_I8;
			var.hVal.QuadPart = cliptime;
			//var.hVal.QuadPart = 5333432;
			IFC( reader->SetCurrentPosition(GUID_NULL, var) );
		}

		// get frame
		DWORD       dwFlags = 0;
		//LONGLONG    hnsTimeStamp = 0;
		//DWORD       cSkipped = 0;           // Number of skipped frames

		//IMFSample *pSample = NULL;

		for (;;)
		{
			IMFSample *pSampleTmp = NULL;


			IFC( reader->ReadSample( (DWORD)MF_SOURCE_READER_FIRST_VIDEO_STREAM,
				0, NULL, &dwFlags, &sampletime, &pSampleTmp) );

			if (dwFlags & MF_SOURCE_READERF_ENDOFSTREAM) break;

			if (dwFlags != 0){
				MessageBox(NULL,"Error","Error", MB_OK);
			}

			if (pSampleTmp == NULL) continue;
			if (cliptime>(UINT64)sampletime && (cliptime - (UINT64)sampletime)>frameInterval / 2){
				SAFE_RELEASE(pSampleTmp);
				continue;
			}

			SAFE_RELEASE(currsample);

			currsample = pSampleTmp;
			currsample->AddRef();
			currTime=sampletime;

			SAFE_RELEASE(pSampleTmp);
			break;
		}
	}

    if (currsample)
    {
        //UINT32 pitch = 4 * framewidth;

        IFC( currsample->ConvertToContiguousBuffer(&pBuffer) );
        IFC( pBuffer->Lock(&pTempData, NULL, &cbBitmapData) );
		memcpy(bitmap, pTempData, cbBitmapData);
		pBuffer->Unlock();
    }
    else
    {
        hr = MF_E_END_OF_STREAM;
    }
	
Cleanup:
    SAFE_RELEASE(pBuffer);
    //SAFE_RELEASE(pSample);

	return hr;
}
