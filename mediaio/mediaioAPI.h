#ifndef _MEDIAIO_API_H_
#define _MEDIAIO_API_H_

/************************************************************************/
/* API class to simplify using mediaio library 
 * */
/************************************************************************/

#include <opencv2/opencv.hpp> 
#include <string>
#include <BaseTsd.h>

#include "mediaio.h"

class mediaioAPI
{
public:
	mediaioAPI(std::string videoPath);
	~mediaioAPI();

	// get video properties
	int get_width() { return width; }
	int get_height() { return height; }
	double get_FPS() { return fps; }
	unsigned long long get_frame_span() { return frame_span; }
	unsigned long long get_num_frames() { return num_frames; }
	unsigned long long get_duration() { return duration; }

	// api
	cv::Mat get_frame_at_time(unsigned long long time); // get frame at given time, time in 10-7s, output: CV_BGR image
	cv::Mat get_frame_at_index(unsigned long long index); // get frame at given frame index, output: CV_BGR image

protected:
private:
	// video properties
	UINT32 width, height, par_numerator, par_denominator, fps_numerator, fps_denominator, num_frames;
	UINT64 duration, frame_span;
	bool topdown;
	double fps;

	// others, for support
	GUID clipID;
	UINT32 size_frame;
};


#endif