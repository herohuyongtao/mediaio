#include "stdafx.h"
#include "mediaioAPI.h"
using namespace std;
using namespace cv;

mediaioAPI::mediaioAPI(string videoPath)
{
	// init
	mioInitializeMediaIO();

	// add video
	HRESULT hr = CoCreateGuid(&clipID);
	wstring widestr = wstring(videoPath.begin(), videoPath.end());
	const wchar_t * video_path = widestr.c_str();
	hr = mioAddClip(clipID, video_path);
	if (hr != S_OK)
	{
		MessageBox(NULL, "Add video failed!!!", "Error!!!", NULL);
		exit(-1);
	}

	// get video properties
	wchar_t idstr[50];
	StringFromGUID2(clipID, idstr, 50);
	hr = mioGetClipInfo(idstr, width, height, par_numerator, par_denominator, fps_numerator, fps_denominator, num_frames, duration, topdown);

	// compute other video properties
	frame_span = mioGetFrameSpanBasedOnFPS(fps_numerator, fps_denominator);
	fps = ((double)10000000) / (double)frame_span;
	size_frame = width*height * 4;
}

mediaioAPI::~mediaioAPI()
{
	// shut down
	mioCleanupAll();
	mioShutdownMediaIO();
}

Mat mediaioAPI::get_frame_at_time(unsigned long long time)
{
	Mat img(height, width, CV_8UC4); // CV_BGRA
	HRESULT hr = mioGetFrameOfClipAt(clipID, (UINT64)time, (UINT8*)img.data, size_frame);
	cvtColor(img, img, CV_BGRA2BGR);

	if (hr != S_OK)
	{
		MessageBox(NULL, "Get frame from the video failed!!!", "Error!!!", NULL);
		exit(-1);
	}
	
	// if rows are not topdown, switch them back
	if (!topdown){
		cv::flip(img, img, 0);
	}

	return img;
}

Mat mediaioAPI::get_frame_at_index(unsigned long long index)
{
	// convert to time first
	unsigned long long time = index*frame_span;
	
	return get_frame_at_time(time);
}