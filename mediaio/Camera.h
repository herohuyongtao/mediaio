#pragma once
#include <list>

#include "Clip.h"

class CCamera
{
public:
	CCamera(std::wstring id, std::wstring cameraname);
	~CCamera(void);

	static CCamera* getCamera(std::wstring id);
	static void delCamera(std::wstring id);
	static void clearAllCameras();

	void addClipToCamera(CClip* clip) { clips.push_back(clip); }
	void removeClipFromCamera(CClip* clip) { clips.remove(clip); }
	CClip* locateClipAtTime(UINT64 unifiedtime);

	std::wstring guid;
	std::wstring name;
	std::list<CClip*> clips;
};

