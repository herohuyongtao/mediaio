#include "StdAfx.h"
#include "Camera.h"

#include <string>
using namespace std;

#include <hash_map>
using namespace stdext;

static hash_map<wstring, CCamera*> cameras;

CCamera::CCamera(wstring id, wstring cameraname) : guid(id), name(cameraname)
{
	cameras[id]=this;
}

CCamera::~CCamera(void)
{
}

CCamera* CCamera::getCamera(wstring id)
{
	return cameras[id];
}

void CCamera::delCamera(wstring id)
{
	//CCamera* camera=cameras[id];
	delete cameras[id];
	cameras[id]=NULL;
}

void CCamera::clearAllCameras()
{
	for(hash_map<wstring, CCamera*>::iterator i=cameras.begin(); i!=cameras.end(); i++) delete (*i).second;
	
	cameras.clear();
}

CClip* CCamera::locateClipAtTime(UINT64 time)
{
	for (list<CClip*>::iterator i=clips.begin(); i!=clips.end(); i++){
		//if (time>=(*i)->startTime && time<(*i)->endTime) return *i;
		CClip* c=*i;
		if (c->compareTime(time, c->startTime)>=0 && c->compareTime(time, c->endTime)<0) return *i;
	}

	return NULL;
}
