// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the MEDIAIO_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// MEDIAIO_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#include <windows.h>
#include <guiddef.h>
#include <basetsd.h>
#include <winnt.h>

// This class is exported from the mediaio.dll
HRESULT mioInitializeMediaIO();
HRESULT mioShutdownMediaIO();
HRESULT mioGetClipInfo(wchar_t* clipID,
								UINT32& framewidth,
								UINT32& frameheight,
								UINT32& par_numerator,
								UINT32& par_denominator,
								UINT32& fps_numerator,
								UINT32& fps_denominator,
								UINT32& framecount,
								UINT64& duration,
								bool& topdown);


HRESULT mioGetFrameAt(GUID camID, UINT64 hnsPos, UINT8* bitmap, UINT32 size);
HRESULT mioGetFrameSize(GUID camID, /*UINT64 hnsPos,*/ UINT32& width, UINT32& height);

HRESULT mioGetFrameOfClipAt(GUID clipID, UINT64 hnsPos, UINT8* bitmap, UINT32 size);

//void mioAddCam(GUID camID, wchar_t* camName);
void mioDelCam(GUID camID);
HRESULT mioAddClip(GUID		clipID,
							const wchar_t*	clipFilePath);
HRESULT mioAddClipWithTimingInfo(GUID		clipID,
							GUID		camID, 
							const wchar_t*	clipFilePath, 
							UINT64		clipStartTime, 
							UINT64		clipEndTime
							);
void mioDelClip(GUID clipID);
void mioUpdateClip(GUID clipID, GUID camID, UINT64 clipStartTime,  UINT64 clipEndTime);
void mioCleanupAll();

HRESULT mioGetThumbnail(wchar_t* clipFilePath, UINT64 thumbnailtime, UINT32& width, UINT32& height, UINT8* bitmap);

/************************************************************************/
/* Save the frame to BMP file to disk
 * */
/************************************************************************/
void mioSaveToBMP(UINT8* data, char* fileName, UINT32 width, UINT32 height, bool topdown);

UINT64 mioGetFrameSpanBasedOnFPS(UINT32 fps_numerator, UINT32 fps_denominator);