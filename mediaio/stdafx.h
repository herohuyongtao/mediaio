// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#pragma warning(disable:4995)	// Disable warning C4995: name was marked as #pragma deprecated
#pragma warning(disable:4533)	// Disable warning C4995: ... is skipped by 'goto xxx'
#pragma warning(disable:4701)	// Disable warning C4701: potentially uninitialized local variable 'xxx' used
#pragma warning(disable:4703)	// Disable warning C4703: potentially uninitialized local pointer variable 'xxx' used

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <propvarutil.h>

#define IFC(x) { hr = (x); if (FAILED(hr)) goto Cleanup; }
#define IFCOOM(x) { if ((x) == NULL) { hr = E_OUTOFMEMORY; IFC(hr); } }
#define SAFE_RELEASE(x) { if (x) { (x)->Release(); (x) = NULL; } }


